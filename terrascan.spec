%define debug_package %{nil}

Name:           terrascan
Version:        1.19.9
Release:        1%{?dist}
Summary:        Detect compliance and security violations across Infrastructure as Code to mitigate risk before provisioning cloud native infrastructure.

License:        ASL 2.0
URL:            https://runterrascan.io/
Source0:        https://github.com/tenable/%{name}/releases/download/v%{version}/%{name}_%{version}_Linux_x86_64.tar.gz

%description
Detect compliance and security violations across Infrastructure 
as Code to mitigate risk before provisioning cloud native infrastructure.

%prep
%autosetup -n %{name} -c

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/usr/bin
install -p -m 755 %{name} %{buildroot}/usr/bin

%files
/usr/bin/terrascan

%changelog
* Mon Dec 09 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Thu Apr 11 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Sun Mar 03 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version v1.18.12
* Thu Dec 28 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version v1.18.11
* Thu Aug 31 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version v1.18.3
* Tue Apr 25 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to v1.18.1
* Fri Feb 10 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to v1.18.0
* Wed Jan 11 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to v1.17.1
* Fri Nov 25 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to v1.17.0
* Thu Oct 20 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to v1.16.0
* Thu Sep 11 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Inital RPM
